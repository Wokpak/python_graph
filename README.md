# Python Undirected Graph

This is a basic python implementation of a uni-directional graph data structure. It makes use of python dictionaries and sets.

The Graph class tracks 'nodes', corresponding 'links' between them and also tracks which are 'edge nodes', having only a single link. Included within the class is a method for assessing if 2 nodes are connected.

Run Example.py to see how the graph is constructed and what functions are available. The following figure shows the structure used.

![alt text](example.png "Logo Title Text 1")