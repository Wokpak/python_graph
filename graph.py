"""
Uni-directional graph data structure
Author:    Jed Hollom
Date:      September 2016
Email: Jedhollom@gmail.com
"""


class Graph():
	"""Undirected graph data structure"""

	def __init__(self, nodes=set()):
		self._graph = {}
		self._edges = set()

		if type(nodes) is str:
			nodes = set(nodes)

		for x in nodes:
			self._graph.update({x: set()})

	def add_nodes(self, nodes):
		"""Add nodes to graph"""
		if type(nodes) is str:
			nodes = set(nodes)

		for x in nodes:
			if x not in self._graph.keys():
				self._graph.update({x: set()})
			else:
				print 'Warning: Key {} already exists in graph'.format(x)

		self._find_edge()

	def add_links(self, links):
		"""Add links between existing nodes"""
		if type(links[0]) is str:
			links = [links]

		for x in links:
			if x[0] not in self._graph.keys() or x[1] not in self._graph.keys():
				raise ValueError('\'{}\' does not exist in graph'.format(x[0]))
			else:
				self._graph[x[0]].add(x[1])
				self._graph[x[1]].add(x[0])

		self._find_edge()

	def remove_nodes(self, nodes):
		"""Remove nodes from graph"""
		if type(nodes) is str:
			nodes = set(nodes)

		for x in nodes:
			for y in self._graph[x]:
				self._graph[y].remove(x)

		self._graph.pop(x)

		self._find_edge()

	def remove_links(self, links):
		"""Remove links to nodes"""
		if type(links[0]) is str:
			links = [links]

		for x in links:
			if x[0] not in self._graph.keys() or x[1] not in self._graph.keys():
				raise ValueError('\'{}\' does not exist in graph'.format(x[0]))

			if x[1] in self._graph[x[0]] and x[0] in self._graph[x[1]]:
				self._graph[x[0]].remove(x[1])
				self._graph[x[1]].remove(x[0])
			else:
				print 'Warning: link does not exist between nodes \'{}\' and \'{}\''.format(x[0], x[1])

		self._find_edge()

	def get_nodes(self):
		"""Return list of nodes in graph"""
		return self._graph.keys()

	def get_links(self, node):
		"""Return links to and from specific node"""
		return self._graph[node]

	def get_graph(self):
		"""Return full graph structure"""
		return self._graph

	def get_edges(self):
		"""Return full graph structure"""
		return self._edges

	def _find_edge(self):
		"""Creates set of nodes that are edges of the graph"""
		for i in self._graph:
			if len(self._graph[i]) == 1:
				self._edges.add(i)
			else:
				if i in self._edges:
					self._edges.remove(i)

	def check_connection(self, start, end):
		"""Checks if connection exists between two nodes"""
		queue = set()
		queue.update(self._graph[start])

		visited = set()
		visited.update(start)

		while len(queue) != 0:
			current_node = queue.pop()
			visited.add(current_node)
			queue.update(self._graph[current_node])
			queue.difference(self._edges)
			queue.difference_update(visited)

		if end in visited:
			return True
		else:
			return False
